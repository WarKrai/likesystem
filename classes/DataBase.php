<?php


class DataBase
{
    private static $host = "localhost";
    private static $database = "likeSystem";
    private static $user = "root";
    private static $password = "";

    public static function connect(): PDO
    {
        $pdo = new PDO('mysql:host='. self::$host .';dbname='.self::$database.';charset=utf8',
            self::$user, self::$password);
        $pdo->exec("SET NAMES utf8");
        return $pdo;
    }
}