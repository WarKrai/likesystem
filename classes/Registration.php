<?php
require_once('DataBase.php');

class Registration
{
    public static function userRegistration(string $email, string $password, int $age = NULL): bool
    {
        $pdo = DataBase::connect();
        $query = $pdo->prepare("SELECT COUNT(*) FROM users WHERE mail = :mail");
        $query->execute(array(':mail'=>$email));
        $result = $query->fetch(PDO::FETCH_ASSOC);
        if (intval($result['COUNT(*)'] == 0)) {
            $query = $pdo->prepare("INSERT INTO users VALUES (NULL, :mail, :password, :age, :dat)");
            $query->execute(array(':mail'=>$email, ":password"=>$password, ":age"=>$age, ":dat"=>date("Y-m-d H:i:s")));
            return true;
        } else {
            return false;
        }
    }

    public static function login(string $email, string $password): bool
    {
        $pdo = DataBase::connect();
        $query = $pdo->prepare("SELECT COUNT(*) FROM users WHERE mail = :mail AND pass = :pass");
        $query->execute(array(':mail'=>$email, ':pass'=>$password));
        $result = $query->fetch(PDO::FETCH_ASSOC);
        if (intval($result['COUNT(*)'] == 1)) {
            return true;
        } else {
            return false;
        }
    }
}