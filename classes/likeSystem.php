<?php
require_once('DataBase.php');

class LikeSystem
{

    public static function getLikesOnPage(int $id, string $page): int
    {
        $pdo = DataBase::connect();
        $query = $pdo->prepare("SELECT COUNT(*) FROM likes WHERE id = :id AND page = :page");
        $query->execute(array(':id'=>$id, ':page'=>$page));
        $result = $query->fetch(PDO::FETCH_ASSOC);
        return intval($result['COUNT(*)']);
    }

    public static function like(int $id, string $page, string $user): array
    {
        $pdo = DataBase::connect();
        $query = $pdo->prepare("SELECT COUNT(*) FROM likes WHERE id = :id AND page = :page AND usr = :usr");
        $query->execute(array(':id'=>$id, ':page'=>$page, ':usr'=>$user));
        $result = $query->fetch(PDO::FETCH_ASSOC);

        if (intval($result['COUNT(*)'] == 1)) {
            return array(
                "error"=>true,
                "message"=>"Вы уже оценили эту страницу!",
                "count"=>self::getLikesOnPage($id, $page)
            );
        } elseif(intval($result['COUNT(*)']) == 0) {
            $query = $pdo->prepare("INSERT INTO likes VALUES (:id, :page, :usr)");
            $query->execute(array(':id'=>$id, ':page'=>$page, ':usr'=>$user));
            return array(
                "error"=>false,
                "message"=>"",
                "count"=>self::getLikesOnPage($id, $page)
            );
        }
    }
}