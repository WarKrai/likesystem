<?php
header('Content-Type: text/json; charset=utf-8');
header("Access-Control-Allow-Origin: *");
require_once('classes/likeSystem.php');

$id = $_POST['id'];
$url = $_POST['url'];
$error = false;
$message = "";

if ($id) {
    $result = array(
        "error" => $error,
        "message" => $message,
        "count" => LikeSystem::getLikesOnPage($id, $url)
    );
} else {
    $error = true;
    $message = "Случилась ошибка!";
    $result = array(
        "error" => $error,
        "message" => $message,
        "count" => 0
    );
}

header('Content-Type: text/json; charset=utf-8');
header("Access-Control-Allow-Origin: *");
echo json_encode($result);