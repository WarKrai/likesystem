<?php
header("Access-Control-Allow-Origin: *");
require_once('classes/likeSystem.php');

$id = $_POST['id'];
$url = $_POST['url'];
$user = $_POST['user'];


if(isset($id) && isset($url) && isset($user)) {
    $result = LikeSystem::like($id, $url, $user);
} else {
    $result = array(
        "error" => true,
        "message" => "Случилась ошибка!",
        'count' => 0
    );
}

header('Content-Type: text/json; charset=utf-8');
echo json_encode($result);
