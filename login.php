<?php
require_once("classes/Registration.php");
require_once("classes/DataBase.php");

if (Registration::login($_POST['mail'], $_POST['pass'])) {
    require('info.php');
    $pdo = DataBase::connect();
    $query = $pdo->prepare("SELECT * FROM users WHERE mail = :mail AND pass = :pass");
    $query->execute(array(":mail"=>$_POST['mail'], ":pass"=>$_POST['pass']));
    $result = $query->fetchAll(PDO::FETCH_ASSOC);
    echo "Ваш id={$result[0]['id']}<br>";
    echo "Ваши лайки: <br>";
    $query = $pdo->prepare("SELECT * FROM likes WHERE id = :id");
    $query->execute(array(':id'=>$result[0]['id']));
    $result = $query->fetchAll(PDO::FETCH_ASSOC);
    $pages = array();
    foreach ($result as $item) {
        array_push($pages, $item['page']);
    }
    $pages = array_unique($pages);
    foreach($pages as $page) {
        $query = $pdo->prepare("SELECT COUNT(*) FROM likes WHERE id = :id AND page = :page");
        $query->execute(array(':id'=>$result[0]['id'], ':page'=>$page));
        $countOfLikes = $query->fetch(PDO::FETCH_ASSOC);
        echo "На странице {$page}: {$countOfLikes['COUNT(*)']} лайков.<br>";
    }
} else {
    require('index.php');
    echo "Ошибка входа";
}